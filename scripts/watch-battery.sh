#!/bin/bash
if [[ "${DESKTOP_SESSION}" != "i3" ]] ; then
  exit
fi

BATTINFO=`acpi -b | grep -v unavailable`
REMAINING_TIME=`echo $BATTINFO | cut -f 5 -d " "`

if [[ `echo $BATTINFO | grep Discharging` && $REMAINING_TIME < 00:10:00 ]] ; then
    DISPLAY=:0.0 /usr/bin/notify-send -u critical "low battery" "$BATTINFO"
fi
