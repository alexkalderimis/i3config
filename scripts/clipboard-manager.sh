#!/bin/sh
lock() {
    i3lock --image=$HOME/Pictures/dolomites.png
}

case "$1" in
    start)
        if which copyq; then
          copyq
        elif which clipster; then
          clipster --daemon
        fi
        ;;
    show)
        if which copyq; then
          copyq show
        elif which clipster; then
          clipster --select --clipboard
        else
          notify-send 'No clipboard manager available'
        fi
        ;;
    *)
        echo "Usage: $0 {start|show}"
        exit 2
esac

exit 0

